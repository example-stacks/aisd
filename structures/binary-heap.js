//no meld, meld is linear we need to rebuild the heap
function BinaryHeap() {

    let size = 0;

    let memory = []

    //o(log(n))
    function insert(elem) {
        memory.push(elem)
        moveUp(size)
        size++;
    }

    //o(n)
    function build(array) {
        memory = array
        size = memory.length;

        for (let i = Math.floor(size / 2); i >= 0; i--) {

            moveDown(i)

        }
    }

    //o(log(n))
    function deleteMax() {
        memory[0] = memory[size - 1];
        moveDown(0);
        size--;
        memory.pop();
    }

    function max() {
        return memory[0]

    }

    function decrease(i, howMuch) {
        memory[i] -= howMuch;
        moveDown(i);
    }

    function increase(i, howMuch) {
        memory[i] += howMuch;
        moveUp(i)

    }

    function moveUp(i) {
        if (i > 0 && memory[Math.floor(i / 2)] < memory[i]) {
            let foo = memory[Math.floor(i / 2)]
            memory[Math.floor(i / 2)] = memory[i]
            memory[i] = foo
            moveUp(Math.floor(i / 2))
        }
    }

    function moveDown(i) {
        let source = i, target = i;
        if (i * 2 < size) {
            if (memory[i * 2] > memory[i]) {
                target = i * 2
            }
            if (memory[i * 2 + 1] > memory[target]) {
                target = i * 2 + 1;
            }
        }
        if (target !== source) {
            let foo = memory[target]
            memory[target] = memory[source]
            memory[source] = foo
            moveDown(target)
        }
    }

    return {
        insert,
        deleteMax,
        max,
        decrease,
        increase,
        build,
    }
}

module.exports = BinaryHeap;
