function UnionFind(initMemory) {


    let memory = [];
    let externalName = [];
    let internalName = [];
    let roots = []
    let pointers = []
    let size = [];

    for (let i = 0; i < initMemory.length; i++) {
        memory[i] = initMemory[i];
        externalName[initMemory[i]] = initMemory[i];
        internalName[initMemory[i]] = initMemory[i];
        roots[initMemory[i]] = i;
        size[initMemory[i]] = 1
    }

    function find(elem) {
        return externalName[memory[elem]]
    }

    function union(SetA, SetB, NewName) {
        let internalSetA = internalName[SetA];
        let internalSetB = internalName[SetB];
        if (size[internalSetA] > size[internalSetB]) {
            internalSetB = internalSetA;
            internalSetA = internalName[SetB]
        }
        let element = roots[internalSetA];
        let last;
        while (element) {
            memory[element] = internalSetB;
            last = element;
            element = pointers[element];
        }

        pointers[last] = roots[internalSetB];
        roots[internalSetB] = roots[internalSetA];
        size[internalSetB] = size[internalSetA] + size[internalSetB];
        internalName[NewName] = internalSetB;
        externalName[internalSetB] = NewName;
    }

    return {
        union,
        find
    }
}

module.exports = UnionFind
