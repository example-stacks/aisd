function matches(text, pattern, start) {
    for (let i = 0; i < pattern.length; i++) {
        if (pattern[i] !== text[i + start]) {
            return false;
        }
    }
    return true;
}

function match(text, pattern) {

    for (let start = 0; start < text.length - pattern.length + 1; start++) {
        if (matches(text, pattern, start)) {
            console.log('Matched at', start)
        }
    }
}

(() => {

    let text = "AAAAABAAABA";
    let pattern = "ABA";

    match(text, pattern)
})();
