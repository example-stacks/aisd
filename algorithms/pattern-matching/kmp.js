function lps(pattern) {
    let lps = [];
    lps[0] = 0;

    let length = 0//previous longest prefix suffix

    let i = 1;
    while (i < pattern.length) {
        if (pattern[i] === pattern[pattern.length - 1]) {
            length += 1;
            lps[i] = length
            i++;
        } else {
            if (length === 0) {
                lps[i] = 0;
                i++;
            } else {
                length = lps[length - 1];
            }
        }
    }

    return lps
}

function findPattern(text, pattern) {

    let lpsArray = lps(pattern);

    let i = 0, j = 0;
    while (i < text.length) {
        if (pattern[j] === text[i]) {
            j++;
            i++;
        }

        if (j === pattern.length) {
            console.log('Pattern on', i - j)
            j = lpsArray[j - 1]
        } else if (i < text.length && pattern[j] !== text[i]) {
            if (j !== 0) {
                j = lpsArray[j - 1]
            } else {
                i++;
            }
        }
    }
}


(() => {

    let text = "AAAAABAAABA";
    let pattern = "ABA";

    findPattern(text, pattern)
})();
