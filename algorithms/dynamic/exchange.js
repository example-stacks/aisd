// for a infinite supply of coins from a given set of values, in how many different ways we can exchange N, order doesnt matter

// the key is to split problem into two subproblems
// we can split the problem into
// A) the number of different ways to solve the problem removing one value from the coins
// B) the number of different ways to solve the problem is the value exists in the solution at least once


// easy implementation will be recursive, but we will endup solving the same subproblems couple of times

// lets eliminate recursion and build the solution bottom-up

function count(coins, allowedCoinsLength, theNumber) { //complexity O(nm), both memory and time

    let solutions = Array.from(Array(theNumber + 1), () => new Array(allowedCoinsLength));

    //solutions[theNumber][allowedCoins]


    for (let i = 0; i < allowedCoinsLength; i++) {
        solutions[0][i] = 1;
    }

    for (let i = 1; i < theNumber + 1; i++) {
        for (let j = 0; j < allowedCoinsLength; j++) {

            let solutionsExcludingCoinJ = j >= 1 ? solutions[i][j - 1] : 0
            let solutionsIncludingCoinJ = i - coins[j] >= 0 ? solutions[i - coins[j]][j] : 0

            solutions[i][j] = solutionsExcludingCoinJ + solutionsIncludingCoinJ;

        }

    }

    return solutions[theNumber][allowedCoinsLength - 1]
}

function count2(coins, allowedCoinsLength, theNumber) { //complexity O(nm) time, O(n) memory

    let solutions = new Array(theNumber + 1);
    solutions.fill(0);

    //solutions[theNumber]


    solutions[0] = 1;

    //for each coin
    //for each solution if the coin fits into the solution add number of solutions without this coin to the table

    for (let i = 0; i < allowedCoinsLength; i++) {
        for (let j = coins[i]; j <= theNumber; j++) {
            solutions[j] += solutions[j - coins[i]]
        }
    }

    console.log(solutions)

    return solutions[theNumber]
}

(async () => {

    let solutions = count2([1, 2, 3], 3, 4)
    console.log(solutions)

})()
