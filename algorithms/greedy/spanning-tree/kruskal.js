const UnionFind = require("../../../structures/union-find");


(async () => {

    let nodes = ["0", "1", "2", "3", "4"]

    let graph = {
        "1": [
            ["2", 4],
            ["3", 5]
        ],
        "2": [
            ["3", 3],
        ],
        "3": [
            ["4", 1]
        ]
    }

    let uf = new UnionFind(nodes)

    let edges = [];

    for (let node in graph) {
        for (let [child, weight] of graph[node]) {
            edges.push([weight, node, child])
        }
    }
    edges.sort(([a], [b]) => a - b)

    let MST = {}
    for (let [weight, from, to] of edges) {
        let fromSet = uf.find(from)
        let toSet = uf.find(to)
        if (fromSet !== toSet) {
            uf.union(fromSet, toSet, toSet)
            MST[from] = MST[from] ? [...MST[from], [to, weight]] : [[to, weight]]
        }
    }


    console.log(MST)


})()
