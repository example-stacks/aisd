function minmax(array) {

    if (array.length === 1) {
        return [array[0], array[0]]
    } else if (array.length === 2) {
        if (array[0] < array[1]) {
            return [array[0], array[1]]
        } else {
            return [array[1], array[0]]
        }
    } else {
        let [min1, max1] = minmax(array.slice(0, Math.floor(array.length) / 2))
        let [min2, max2] = minmax(array.slice(Math.floor(array.length) / 2, array.length))

        return [
            min1 < min2 ? min1 : min2,
            max1 < max2 ? max2 : max1,
        ]
    }

}


(() => {

    let array = [4, 51, 12, 7, 5, 34, 234, 2, -1, 4, 6, 7674, 53, 452, 3, 2, 3424];

    console.log(minmax(array))
})()
