function findPivot(array) {
    return array[Math.floor(Math.random() * array.length)]
}

//O(N) with randomised data
function selection(k, array) {
    if (array.length < 5) {
        array.sort();
        return array[k];
    } else {
        let pivot = findPivot(array);
        let smallerThanPivot = [], equalOrLargerThanPivot = [];
        for (let elem of array) {
            elem < pivot ? smallerThanPivot.push(elem) : equalOrLargerThanPivot.push(elem)
        }

        if (k < smallerThanPivot.length) {
            return selection(k, smallerThanPivot)
        } else {
            return selection(k - smallerThanPivot.length, equalOrLargerThanPivot)
        }
    }
};

(() => {

    let array = [4, 51, 12, 7, 5, 34, 234, 2, -1, 4, 6, 7674, 53, 452, 3, 2, 3424];

    console.log(selection(6, array))
})()
