function select(array) {
    for (let i = 0; i < array.length; i++) {

        let minPosition = i, theMin = array[i];
        for (let j = i + 1; j < array.length; j++) {
            if (array[j] < theMin) {
                theMin = array[j];
                minPosition = j;
            }
        }
        array[minPosition] = array[i];
        array[i] = theMin;

    }
}

(async () => {

    let array = [4, 51, 12, 7, 5, 34, 234, 2, 2, 4, 6, 7674, 53, 452, 3, 2, 3424];

    select(array);

    console.log(array)

})()

// time complexity o(n^2)
