function partition(items, left, right) {
    let pivot = items[Math.floor((right + left) / 2)], //middle element
        i = left, //left pointer
        j = right; //right pointer
    while (i <= j) {
        while (items[i] < pivot) {
            i++;
        }
        while (items[j] > pivot) {
            j--;
        }
        if (i <= j) {
            let foo = items[j];
            items[j] = items[i];
            items[i] = foo;
            i++;
            j--;
        }
    }
    return i;
}

function quicksort(array, start, end) {

    console.log('aa', start, end)

    if (end - start >= 1) { //here if small better would be insert-sort
        let pivotPosition = partition(array, start, end)

        quicksort(array, start, pivotPosition - 1);
        quicksort(array, pivotPosition + 1, end);
    }


}


(() => {

    let array = [4, 51, 6, 3];

    quicksort(array, 0, array.length - 1)

    console.log(array)


    // let pivot = partition(array, 0, 17-1, 6)
    // console.log(pivot)
    // console.log(array)
})()
