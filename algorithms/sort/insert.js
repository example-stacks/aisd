function insert(array) {
    for (let i = 1; i < array.length; i++) {
        let elem = array[i], j = i - 1;
        while (j >= 0 && elem < array[j]) {
            array[j + 1] = array[j];
            j--;
        }
        array[j + 1] = elem;
    }
}

(async () => {

    let array = [4, 51, 12, 7, 5, 34, 234, 2, 2, 4, 6, 7674, 53, 452, 3, 2, 3424];

    insert(array);

    console.log(array)

})()

//time complexity o(n^2) if table presorted from high to low o(n) if table sorted
