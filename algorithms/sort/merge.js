function merge(subA, subB) {
    let result = []
    let pointerA = 0;
    let pointerB = 0;

    while (pointerA < subA.length && pointerB < subB.length) {
        if (subA[pointerA] < subB[pointerB]) {
            result.push(subA[pointerA])
            pointerA++
        } else {
            result.push(subB[pointerB])
            pointerB++
        }
    }
    while (pointerA < subA.length) {
        result.push(subA[pointerA])
        pointerA++
    }
    while (pointerB < subB.length) {
        result.push(subB[pointerB])
        pointerB++
    }

    return result;
}

function mergesort(array) {

    if (array.length > 1) { //would be more optimal to do insert sort when length is small
        let subA = array.slice(0, Math.floor(array.length / 2))
        let subB = array.slice(Math.floor(array.length / 2), array.length)
        subA = mergesort(subA);
        subB = mergesort(subB);
        return merge(subA, subB)
    } else {
        return array;
    }
}

(async () => {

    let array = [4, 51, 12, 7, 5, 34, 234];


    console.log(mergesort(array))


})()

//time complexity o(n^2) if table presorted from high to low o(n) if table sorted
